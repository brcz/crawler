// +build unit

package page

import (
	"net/url"
	"strings"
	"testing"

	"sort"

	"github.com/stretchr/testify/assert"
)

func TestNewSingleSite(t *testing.T) {
	url, _ := url.Parse("https://example.com/contact_us/section")
	site := NewSingleSite(url)
	assert.Implements(t, (*Parser)(nil), site, "Must implements Parser interface")
	assert.Equal(t, "https://example.com", site.baseURL)
}

func TestParse(t *testing.T) {
	url, _ := url.Parse("https://example.com/contact_us/section")
	site := NewSingleSite(url)

	expected := []string{"https://example.com", "https://example.com/about"}
	sample := strings.NewReader(` <p>
	<a href="https://example.com">Root</a>
	<a href='https://news.google.com'>skip it</a>
	<a style=\"\" href=https://imgur.com>3</a>
	http://dontcollectthis.com
	<a href="/about#contact"> Contact us</a>
	</p>`)

	links := site.Parse(sample)
	sort.Strings(links)
	assert.Equal(t, links, expected)
}
func TestLocalLink(t *testing.T) {
	testcases := []struct {
		input    string
		expected string
		status   bool
	}{
		{
			"/goodies/",
			"https://shop.store/goodies",
			true,
		},
		{
			"https://shop.store/about",
			"https://shop.store/about",
			true,
		},
		{
			"https://applestore.com",
			"",
			false,
		},
	}
	for _, tc := range testcases {
		t.Run(tc.input, func(t *testing.T) {
			parser := &SingleSitePages{
				baseURL: "https://shop.store/",
			}
			got, status := parser.localLink(tc.input)
			assert.Equal(t, tc.expected, got)
			assert.Equal(t, tc.status, status)
		})
	}
}
func TestUniq(t *testing.T) {
	testcases := []struct {
		name     string
		input    []string
		expected []string
	}{
		{
			"Empty",
			[]string{},
			[]string{},
		},
		{
			"Simplest",
			[]string{"a"},
			[]string{"a"},
		},
		{
			"Uniq",
			[]string{"a", "b", "d", "e", "c"},
			[]string{"a", "b", "c", "d", "e"},
		},
		{
			"Duplicates",
			[]string{"a", "b", "d", "e", "c", "a", "b", "d", "e", "c", "a", "b", "d", "e", "c"},
			[]string{"a", "b", "c", "d", "e"},
		},
	}
	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			got := uniq(tc.input)
			sort.Strings(got)
			assert.Equal(t, tc.expected, got)
		})
	}
}
func TestTrimHash(t *testing.T) {
	testcases := []struct {
		input    string
		expected string
	}{
		{
			"auto",
			"auto",
		},
		{
			"/page#",
			"/page",
		},
		{
			"/local#anchor",
			"/local",
		},
		{
			"http://example.com/page.html#top",
			"http://example.com/page.html",
		},
	}
	for _, tc := range testcases {
		t.Run(tc.input, func(t *testing.T) {
			got := trimHash(tc.input)
			assert.Equal(t, tc.expected, got)
		})
	}
}
