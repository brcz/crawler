// +build unit

package sitemap

import (
	"sort"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBackendInterfaceImplementation(t *testing.T) {
	testcases := []struct {
		name        string
		underlaying interface{}
	}{
		{
			"sync.map",
			&syncedMap{},
		},
		{
			"mutex.map",
			&mutexMap{},
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			assert.Implements(t, (*Backend)(nil), tc.underlaying)
		})
	}
}

func TestNewInMem(t *testing.T) {
	testcases := []struct {
		name        string
		cores       int
		underlaying interface{}
	}{
		{
			"sync.map",
			CoresTreshold,
			&syncedMap{},
		},
		{
			"mutex.map",
			-1,
			&mutexMap{},
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			b := NewInMem(tc.cores)
			assert.IsType(t, tc.underlaying, b)
		})
	}

}

func TestSyncMem(t *testing.T) {
	b := NewInMem(CoresTreshold)
	wg := sync.WaitGroup{}
	for _, str := range inputSample() {
		wg.Add(1)
		go func(sample string) {
			defer wg.Done()
			b.Append(sample)
		}(str)
	}
	wg.Wait()
	got, hasMore := b.Next()
	sort.Strings(got)
	assert.Equal(t, uint64(27), b.Size())
	assert.False(t, hasMore)
	assert.Equal(t, expectedAlphabet, got)
}

func TestMutexMap(t *testing.T) {
	b := NewInMem(-1) //force to use mutex protected map
	wg := sync.WaitGroup{}
	for _, str := range inputSample() {
		wg.Add(1)
		go func(sample string) {
			defer wg.Done()
			b.Append(sample)
		}(str)
	}
	wg.Wait()
	got, hasMore := b.Next()
	sort.Strings(got)
	assert.Equal(t, uint64(27), b.Size())
	assert.False(t, hasMore)
	assert.Equal(t, expectedAlphabet, got)
}
