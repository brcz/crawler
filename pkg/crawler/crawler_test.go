package crawler

import (
	"net/http"
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewBotDefaults(t *testing.T) {
	b := NewBot("", nil, nil, nil)
	assert.NotNil(t, b)
	assert.NotNil(t, b.client)
	assert.Nil(t, b.parser)
	assert.Nil(t, b.out)
	assert.Equal(t, b.userAgent, defaultUserAgent)
}

func TestDefaultOverrides(t *testing.T) {
	b := NewBot("mock bot", nil, nil, mockHTTPClient())
	assert.Equal(t, b.userAgent, "mock bot")
	client := &http.Client{
		Transport: &mockTransport{},
	}
	assert.Equal(t, b.client, client)
}

func TestGet(t *testing.T) {
	b := NewBot("mock bot", nil, nil, mockErrorHTTPClient())
	assert.NotPanics(t, func() {
		b.Get("http://localhost")
	}, "Should not panic due http transport error.It has to ignore it")
	b.client = mockHTTPClient()
	assert.Panics(t, func() {
		b.Get("http://localhost")
	}, "Should  panic due uninitialised parser")
	b.parser = &mockParser{}
	out := make(chan string)
	b.out = out
	resp := make([]string, 0, len(mockParserLinks))
	go func() {
		for i := 0; i < len(mockParserLinks); i++ {
			val := <-out
			resp = append(resp, val)
		}
	}()
	assert.NotPanics(t, func() {
		b.Get("http://localhost")
		close(out)
	})
	sort.Strings(resp)
	assert.Equal(t, mockParserLinks, resp)
}
