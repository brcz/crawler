package crawler

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

func mockHTTPClient() *http.Client {
	return &http.Client{
		Transport: &mockTransport{},
	}
}

func mockErrorHTTPClient() *http.Client {
	return &http.Client{
		Transport: &mockTransportError{},
	}
}

type mockTransport struct{}
type mockTransportError struct{}

// Implement http.RoundTripper
func (*mockTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	// Create mocked http.Response
	response := &http.Response{
		Header:     make(http.Header),
		Request:    req,
		StatusCode: http.StatusOK,
	}
	response.Header.Set("Content-Type", "text/html")
	responseBody := mockBodyContent
	response.Body = ioutil.NopCloser(strings.NewReader(responseBody))
	return response, nil
}

func (*mockTransportError) RoundTrip(req *http.Request) (*http.Response, error) {
	return nil, fmt.Errorf("oups error")
}

var (
	mockBodyContent = `
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
`
	mockParserLinks = []string{"http://example.com", "http://example.com/about", "http://example.com/home"}
)

type mockParser struct{}

func (*mockParser) Parse(input io.Reader) []string {
	return mockParserLinks
}
